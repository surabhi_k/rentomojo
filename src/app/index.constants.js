/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('code')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
